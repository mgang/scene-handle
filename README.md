# 《场景之解决方案》

### 介绍
为记录在工作中遇到的一些比较有典型场景的问题特地记录其解决方案，姑且称之为《场景之解决方案》

### 计划
1. ~~如何加快处理速度？~~
2. 面对内存泄漏，将如何排查代码漏洞？
3. 面对高并发按钮，如何提升应用高并发能力？
