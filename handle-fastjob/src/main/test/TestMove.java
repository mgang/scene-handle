import org.junit.Test;
import org.junit.runner.RunWith;
import org.mango.scene.fastjob.Startup;
import org.mango.scene.fastjob.executor.MainExecutor;
import org.mango.scene.fastjob.service.IYesoulChenYuService;
import org.mango.scene.fastjob.thread.WorkThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by meigang on 2019/9/4.
 */
@SpringBootTest(classes = {Startup.class})
@RunWith(SpringRunner.class)
public class TestMove {
    @Autowired
    @Qualifier("yesoulChenYuServiceImpl")
    private IYesoulChenYuService yesoulChenYuService;
    @Autowired
    private MainExecutor mainExecutor;
    @Test
    public void xx(){
        //yesoulChenYuService.moveData();

        mainExecutor.addTask(new WorkThread());

    }
}
