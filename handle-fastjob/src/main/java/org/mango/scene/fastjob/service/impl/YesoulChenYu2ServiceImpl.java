package org.mango.scene.fastjob.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.mango.scene.fastjob.entity.TargetYesoulChenYu;
import org.mango.scene.fastjob.mapper.TargetYesoulChenYuMapper;
import org.mango.scene.fastjob.service.IYesoulChenYu2Service;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mango
 * @since 2019-09-03
 */
@Service
@DS("target")
public class YesoulChenYu2ServiceImpl extends ServiceImpl<TargetYesoulChenYuMapper, TargetYesoulChenYu> implements IYesoulChenYu2Service {


    @Override
    public void saveData(TargetYesoulChenYu targetYesoulChenYu) {
        this.save(targetYesoulChenYu);
    }
}
