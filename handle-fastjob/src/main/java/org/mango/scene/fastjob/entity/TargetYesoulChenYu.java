package org.mango.scene.fastjob.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author mango
 * @since 2019-09-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TargetYesoulChenYu")
public class TargetYesoulChenYu implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("Id")
    private String Id;

    @TableField("ChengYu")
    private String ChengYu;

    @TableField("PingYin")
    private String PingYin;

    @TableField("DianGu")
    private String DianGu;

    @TableField("ChuChu")
    private String ChuChu;

    @TableField("LiZi")
    private String LiZi;

    @TableField("SPingYin")
    private String SPingYin;

    @TableField("Flag")
    private String Flag;


}
