package org.mango.scene.fastjob;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan("org.mango")
@MapperScan({"org.mango"})
@EnableTransactionManagement
@EnableScheduling
//@EnableDiscoveryClient
public class Startup implements ApplicationListener<WebServerInitializedEvent> {
    Logger logger = LoggerFactory.getLogger(Startup.class);
    public static void main(String[] args) {
        SpringApplication.run(Startup.class, args);
    }

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        logger.info("handle启动成功，监听在端口"+event.getWebServer().getPort());
    }
}
