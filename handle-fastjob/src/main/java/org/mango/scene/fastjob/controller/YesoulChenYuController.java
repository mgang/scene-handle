package org.mango.scene.fastjob.controller;


import org.mango.scene.fastjob.executor.MainExecutor;
import org.mango.scene.fastjob.thread.WorkThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author mango
 * @since 2019-09-03
 */
@RestController
@RequestMapping("/fastjob/yesoul-chen-yu")
public class YesoulChenYuController {
    @Autowired
    MainExecutor mainExecutor;
    @RequestMapping("/test")
    public String test(@RequestParam int size){
        for(int i=0;i<size;i++){
            mainExecutor.addTask(new WorkThread());
        }
        return "开启测试，任务数:"+size;
    }
}

