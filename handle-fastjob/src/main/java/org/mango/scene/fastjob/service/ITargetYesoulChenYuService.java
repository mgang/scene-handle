package org.mango.scene.fastjob.service;

import org.mango.scene.fastjob.entity.TargetYesoulChenYu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mango
 * @since 2019-09-04
 */
public interface ITargetYesoulChenYuService extends IService<TargetYesoulChenYu> {

}
