package org.mango.scene.fastjob.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.mango.scene.fastjob.entity.TargetYesoulChenYu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mango
 * @since 2019-09-04
 */
@DS("target")
public interface TargetYesoulChenYuMapper extends BaseMapper<TargetYesoulChenYu> {

}
