package org.mango.scene.fastjob.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import org.mango.scene.fastjob.entity.TargetYesoulChenYu;
import org.mango.scene.fastjob.entity.YesoulChenYu;
import org.mango.scene.fastjob.executor.MainExecutor;
import org.mango.scene.fastjob.mapper.YesoulChenYuMapper;
import org.mango.scene.fastjob.service.IYesoulChenYu2Service;
import org.mango.scene.fastjob.service.IYesoulChenYuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mango
 * @since 2019-09-03
 */
@Service
public class YesoulChenYuServiceImpl extends ServiceImpl<YesoulChenYuMapper, YesoulChenYu> implements IYesoulChenYuService {

    @Autowired
    @Qualifier("yesoulChenYu2ServiceImpl")
    private IYesoulChenYu2Service yesoulChenYu2Service;

    @Autowired
    private YesoulChenYuMapper yesoulChenYuMapper;

    @Override
    public void moveData() {
        List<YesoulChenYu> list = null;
        synchronized (this) {
            //1.先查询数据 线程锁
            list = yesoulChenYuMapper.selectList();
            System.out.println(JSONObject.toJSONString(list));
            for (YesoulChenYu yesoulChenYu : list) {
                //1.1.将状态修改为正在同步中 1
                YesoulChenYu update = new YesoulChenYu();
                update.setId(yesoulChenYu.getId());
                update.setFlag("1");
                this.updateById(update);
            }
        }
        for (YesoulChenYu yesoulChenYu : list) {
            //2.保存到目标库
            TargetYesoulChenYu t = new TargetYesoulChenYu();
            BeanUtils.copyProperties(yesoulChenYu, t);
            yesoulChenYu2Service.saveData(t);
            //3.修改同步标识
            YesoulChenYu update = new YesoulChenYu();
            update.setId(yesoulChenYu.getId());
            update.setFlag("2");
            this.updateById(update);
        }
    }
}
