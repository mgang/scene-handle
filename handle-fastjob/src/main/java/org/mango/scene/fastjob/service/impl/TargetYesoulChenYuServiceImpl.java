package org.mango.scene.fastjob.service.impl;

import org.mango.scene.fastjob.entity.TargetYesoulChenYu;
import org.mango.scene.fastjob.mapper.TargetYesoulChenYuMapper;
import org.mango.scene.fastjob.service.ITargetYesoulChenYuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mango
 * @since 2019-09-04
 */
@Service
public class TargetYesoulChenYuServiceImpl extends ServiceImpl<TargetYesoulChenYuMapper, TargetYesoulChenYu> implements ITargetYesoulChenYuService {

}
