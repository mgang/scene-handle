package org.mango.scene.fastjob.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by meigang on 2019/9/3.
 */
@Component
public class MainExecutor {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private ThreadPoolExecutor executor;
    private BlockingQueue blockingQueue;
    private int initNum = 3;
    private int maxNum = 10;
    private int queueSize = 100;
    private int keepAliveTime = 60;//单位秒

    @PostConstruct
    protected void init(){
        blockingQueue = new ArrayBlockingQueue<>(queueSize);
        executor = new ThreadPoolExecutor(initNum, maxNum, keepAliveTime,
                TimeUnit.SECONDS, blockingQueue,
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    /**
     * 添加任务
     * @param runnable
     */
    public void addTask(Runnable runnable){
        log.info("线程池ThreadPoolExecutor-ArrayBlockingQueue队列当前大小："+blockingQueue.size());
        executor.execute(runnable);
    }



}
