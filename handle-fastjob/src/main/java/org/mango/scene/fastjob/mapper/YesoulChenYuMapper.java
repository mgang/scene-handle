package org.mango.scene.fastjob.mapper;

import org.apache.ibatis.annotations.Select;
import org.mango.scene.fastjob.entity.YesoulChenYu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mango
 * @since 2019-09-03
 */
public interface YesoulChenYuMapper extends BaseMapper<YesoulChenYu> {

    @Select("select * from YesoulChenYu where Flag='0' order by id asc limit 0,10")
    List<YesoulChenYu> selectList();
}
