package org.mango.scene.fastjob.thread;

import org.mango.scene.fastjob.service.IYesoulChenYuService;
import org.mango.scene.fastjob.util.SpringUtil;

/**
 * Created by meigang on 2019/9/3.
 */
public class WorkThread implements Runnable {

    private IYesoulChenYuService yesoulChenYuService =
            (IYesoulChenYuService) SpringUtil.getBean("yesoulChenYuServiceImpl");

    @Override
    public void run() {
        yesoulChenYuService.moveData();
    }
}
