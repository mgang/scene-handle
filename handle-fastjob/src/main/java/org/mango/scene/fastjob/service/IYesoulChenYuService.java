package org.mango.scene.fastjob.service;

import org.mango.scene.fastjob.entity.YesoulChenYu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mango
 * @since 2019-09-03
 */
public interface IYesoulChenYuService extends IService<YesoulChenYu> {

    void moveData();
}
