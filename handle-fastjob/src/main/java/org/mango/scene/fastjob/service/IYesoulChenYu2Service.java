package org.mango.scene.fastjob.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.mango.scene.fastjob.entity.TargetYesoulChenYu;
import org.mango.scene.fastjob.entity.YesoulChenYu;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mango
 * @since 2019-09-03
 */
public interface IYesoulChenYu2Service extends IService<TargetYesoulChenYu> {

    void saveData(TargetYesoulChenYu targetYesoulChenYu);
}
